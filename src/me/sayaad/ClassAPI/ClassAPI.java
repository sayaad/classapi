package me.sayaad.ClassAPI;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import me.sayaad.ClassAPI.classes.FullClass;
import me.sayaad.ClassAPI.classes.NullAbstractClass;
import me.sayaad.ClassAPI.constants.ClassAPIFiles;
import me.sayaad.ClassAPI.constants.DefaultValues;
import me.sayaad.ClassAPI.constants.Utils;
import me.sayaad.ClassAPI.metrics.MetricsLite;
import me.sayaad.ClassAPI.serialization.SerializableArmourItemStack;
import me.sayaad.ClassAPI.serialization.SerializableItemStack;
import me.sayaad.ClassAPI.serialization.SerializablePotionEffect;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;

/**
 * The main ClassAPI class.
 * @author Sayaad
 * @since 2013
 */
public class ClassAPI extends JavaPlugin{

	private HashMap<String, me.sayaad.ClassAPI.classes.Class> classes = new HashMap<String, me.sayaad.ClassAPI.classes.Class>();
	private ClassAPIFiles files;
	
	/**
	 * Construct a ClassAPI instance using the default configuration path, "plugins/ClassAPI/"
	 */
	public ClassAPI(){
		this("");
	}
	
	/**
	 * Construct a ClassAPI instance using a specified directory to generate/load/save configuration files.
	 * @param configPath The directory to manage configuration files.
	 */
	public ClassAPI(String configPath){
		files = new ClassAPIFiles(configPath);
		FullClass.api = this;
		ConfigurationSerialization.registerClass(FullClass.class, "FullClass");
		ConfigurationSerialization.registerClass(SerializableItemStack.class, "Item");
		ConfigurationSerialization.registerClass(SerializableArmourItemStack.class, "Armour");
		ConfigurationSerialization.registerClass(SerializablePotionEffect.class, "Potion");
	}
	
	/**
	 * Starts Metrics.
	 */
	public void onEnable(){
		try {
			new MetricsLite(this).start();
		} catch (IOException e) {}
	}
	
	/**
	 * Get a Class with the specified name in the default Classes.yml file in the directory specified in the constructor of the ClassAPI.
	 * @param name Name of the class.
	 * @return me.sayaad.ClassAPI.classes.Class
	 * @throws ClassNotFoundException Thrown when no class of that name is found.
	 */
	public synchronized me.sayaad.ClassAPI.classes.Class getClass(String name) throws me.sayaad.ClassAPI.exceptions.ClassNotFoundException{
		getFiles().getMainConfig();
		return getClass(getFiles().getMainConfigFile(), name);
	}
	
	/**
	 * Get a class by a Player's inventory and their current active PotionEffects. Created along with a specified permission and teamName in which the created class's name would be that of the player.
	 * @param p Player to obtain class from.
	 * @param permission Permission node for the class.
	 * @param team The team of the class, if any.
	 * @return me.sayaad.ClassAPI.classes.Class
	 */
	public synchronized me.sayaad.ClassAPI.classes.Class getClass(Player p, String permission, String team){
		return getClass(p, permission, p.getName(), team);
	}
	
	/**
	 * Get a class by a Player's inventory and their current active PotionEffects. Created along with a specified permission, name and teamName.
	 * @param p Player to obtain the class from.
	 * @param permission Permission node for the class.
	 * @param name The name of the class.
	 * @param team The team of the class, if any.
	 * @return me.sayaad.ClassAPI.classes.Class
	 */
	public synchronized me.sayaad.ClassAPI.classes.Class getClass(Player p, String permission, String name, String team){
		getFiles().getMainConfig();
		return getClass(p, permission, name, p.getName() + "'s class.", team);
	}
	
	/**
	 * Get a class by a Player's inventory and their current active PotionEffects. Created along with a specified permission, name, description and team.
	 * @param p Player to obtain the class from.
	 * @param permission Permission node for the class.
	 * @param name The name of the class.
	 * @param team The team of the class, if any.
	 * @return me.sayaad.ClassAPI.classes.Class
	 */
	public synchronized me.sayaad.ClassAPI.classes.Class getClass(Player p, String permission, String name, String description ,String team){
		getFiles().getMainConfig();
		ItemStack[] items = (p.getInventory().getContents().equals(DefaultValues.getDefaultItemStack(DefaultValues.DEFAULT_ITEMS, this)))? DefaultValues.getDefaultItemStack(DefaultValues.DEFAULT_ITEMS, this) : p.getInventory().getContents();
		ItemStack[] armour = (p.getInventory().getArmorContents().equals(DefaultValues.getDefaultItemStack(DefaultValues.DEFAULT_ARMOUR, this)))? DefaultValues.getDefaultItemStack(DefaultValues.DEFAULT_ARMOUR, this) : p.getInventory().getArmorContents();
		PotionEffect[] potionEffects = (p.getActivePotionEffects().toArray(new PotionEffect[p.getActivePotionEffects().size()]).equals(DefaultValues.getDefaultPotionEffects(this)))? DefaultValues.getDefaultPotionEffects(this) : p.getActivePotionEffects().toArray(new PotionEffect[p.getActivePotionEffects().size()]); 
		return new FullClass(this, permission, name, description, team, items, armour, potionEffects);
	}
	
	/**
	 * Get a Class with the specified name a specified YAML file.
	 * @param f File to obtain the class from.
	 * @param name Name of the class.
	 * @return me.sayaad.ClassAPI.classes.Class
	 * @throws ClassNotFoundException Thrown when no class of that name is found.
	 */
	@SuppressWarnings("static-access")
	public synchronized me.sayaad.ClassAPI.classes.Class getClass(File f, String name) throws me.sayaad.ClassAPI.exceptions.ClassNotFoundException{
		getFiles().getMainConfig();
		try{
			if(classes.containsKey(name))
				return classes.get(name);
			YamlConfiguration config = new YamlConfiguration();
			if(!getFiles().checkFile(new File(f.getParent()), f))
				Utils.getInstance().log.warning("{ClassAPI} The configuration file does not exist! Creating...");
			if(getFiles().isEmpty(f)){
				Utils.getInstance().log.warning("{ClassAPI} The configuration file is empty!");
				throw new ClassNotFoundException("Class '" + name + "' does not exist!");
			}else config.load(f);
			if(config.get("Classes." + name) == null)
				throw new me.sayaad.ClassAPI.exceptions.ClassNotFoundException("Class '" + name + "' does not exist!");
			me.sayaad.ClassAPI.classes.Class c = (FullClass) config.get("Classes." + name);
			classes.put(name, c);
			return c;
		}catch(Exception e){
			Utils.getInstance().log.severe("{ClassAPI} An error occured loading the class '" + name + "': ");
			e.printStackTrace();
			return new NullAbstractClass(this);
		}
	}
	
	/**
	 * Thread safe method to obtain the ClassAPIFile Manager for the particular ClassAPI.
	 * @return ClassAPIFiles
	 */
	public synchronized ClassAPIFiles getFiles(){
		return files;
	}
	
	/**
	 * Saves a class to the default Classes.yml file located in the directory specified by the constructor of the ClassAPI.
	 * @param c Class to serialise and save save.
	 */
	public synchronized void saveClass(me.sayaad.ClassAPI.classes.Class c){
		saveClass(getFiles().getMainConfigFile(), c);
	}
	
	/**
	 * Saves a class to the specified YAML File.
	 * @param f File to save the class to.
	 * @param c Class to serialise and save.
	 */
	@SuppressWarnings("static-access")
	public synchronized void saveClass(File f, me.sayaad.ClassAPI.classes.Class c){
		try {
			FileConfiguration config = YamlConfiguration.loadConfiguration(f);
			config.set("Classes." + c.getName(), c);
			config.save(f);
		} catch (Exception e) {
			Utils.getInstance().log.severe("{ClassAPI} An error occured saving the class '" + c.getName() + "' to the file '" + f.getName() + "': ");
			e.printStackTrace();
		}
	}
}