package me.sayaad.ClassAPI.classes;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import me.sayaad.ClassAPI.ClassAPI;
import me.sayaad.ClassAPI.constants.ClassAPIFiles;
import me.sayaad.ClassAPI.constants.DefaultValues;
import me.sayaad.ClassAPI.constants.Utils;
import me.sayaad.ClassAPI.serialization.SerializableArmourItemStack;
import me.sayaad.ClassAPI.serialization.SerializableItemStack;
import me.sayaad.ClassAPI.serialization.SerializablePotionEffect;

import org.bukkit.Material;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

/**
 * Full featured class.
 * @author Sayaad
 * @since 2013
 */

@SerializableAs("FullClass")
public class FullClass implements Class{

	public static transient ClassAPI api;
	private final String permission, name, description, team;
	private final ItemStack[] items, armour;
	private final PotionEffect[] potionEffects;
	
	/**
	 * Construct a full featured class.
	 * @param permission Permission node required to obtain the class.
	 * @param name The name of the class.
	 * @param discription A description of the class.
	 * @param team The name of the team, if any.
	 * @param items Items to be given.
	 * @param armour Armour to be equipped, if any.
	 * @param potionEffects PotionEffects to be applied to the player, if any.
	 */
	@SuppressWarnings("static-access")
	public FullClass(ClassAPI api, String permission, String name, String description, String team, ItemStack[] items, ItemStack[] armour, PotionEffect[] potionEffects){
		this.api = api;
		this.permission = permission;
		this.name = name;
		this.description = description;
		this.team = team;
		this.items = items;
		this.armour = armour;
		this.potionEffects = potionEffects;
	}
	
	/**
	 * Gets the permission node configured for the specific class, cannot be null.
	 * @return String
	 */
	@Override
	public String getPermissionNode() {
		return permission;
	}

	/**
	 * Gets the name configured for the specific class, cannot be null.
	 * @return String
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * Gets the description configured for the specific class.
	 * @return String
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the teamName configured for the specific class.
	 * @return String
	 */
	@Override
	public String getTeam() {
		return team;
	}

	/**
	 * Gets the various ItemStacks listed under, "Items" in the configuration file, cannot be null.
	 * @return ItemStack[]
	 */
	@Override
	public ItemStack[] getItems() {
		return items;
	}

	/**
	 * Gets the various ItemStacks listed under, "Armour" in the configuration file, cannot be null.
	 * @return ItemStack[]
	 */
	@Override
	public ItemStack[] getArmour() {
		return armour;
	}

	/**
	 * Gets the various Potions listed under, "Potions" in the configuration file, as an org.bukkit.potion.PotionEffect
	 * @see <a href=http://jd.bukkit.org/rb/apidocs/org/bukkit/potion/PotionEffect.html>
	 * @return PotionEffect[]
	 */
	@Override
	public PotionEffect[] getPotionEffects() {
		return potionEffects;
	}
	
	/**
	 * Convert the FullClass implementation into a ConfigurationSerializeable Map<String, Integer>.
	 * @return Map<String, Integer>
	 */
	@Override
	public synchronized Map<String, Object> serialize() {
		Map<String, Object> c = new LinkedHashMap<String, Object>();
		List<SerializableItemStack> items = new ArrayList<>();
		List<SerializableArmourItemStack> armour = new ArrayList<>();
		List<SerializablePotionEffect> potionEffects = new ArrayList<>();
		for(ItemStack is : getItems())
			if(is != null)
				if(is.getType() != Material.AIR && is.getAmount() > 0)
					items.add(new SerializableItemStack(is));
		for(ItemStack is : getArmour())
			if(is != null)
				if(is.getType() != Material.AIR  && is.getAmount() > 0)
					armour.add(new SerializableArmourItemStack(is));
		for(PotionEffect pe : getPotionEffects())
			if(pe != null)
				potionEffects.add(new SerializablePotionEffect(pe));
		c.put("Permission", getPermissionNode());
		c.put("Name", getName());
		if(!getDescription().equals(DefaultValues.getDefaultString(DefaultValues.DEFAULT_DESCRIPTION, api)));
			c.put("Description", getDescription());
		if(!getTeam().equals(DefaultValues.getDefaultString(DefaultValues.DEFAULT_TEAM, api)))
			c.put("Team", getTeam());
		c.put("Items", items);
		if(!armour.isEmpty())
			c.put("Armour", armour);
		if(!potionEffects.isEmpty())
			c.put("Potion Effects", potionEffects);
		return c;
	}
	
	/**
	 * Create a new FullClass instance from a ConfigurationSerializeable Map.
	 * @param args Map to convert.
	 * @return FullClass
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public static FullClass deserialize(Map<String, Object> args){
		final ClassAPIFiles files = api.getFiles();
		List<ItemStack> items = new ArrayList<ItemStack>(), armour = new ArrayList<ItemStack>();
		List<PotionEffect> potionEffects = new ArrayList<PotionEffect>();
		String  permission = files.getDefaultConfig().getString("Permission"),
				name = files.getDefaultConfig().getString("Name"),
				description = files.getDefaultConfig().getString("Description"),
				team = files.getDefaultConfig().getString("Team");
		if(args.containsKey("Permission"))
			permission = (String)args.get("Permission");
		if(args.containsKey("Name"))
			name = (String)args.get("Name");
		if(args.containsKey("Description"))
			description = (String)args.get("Description");
		if(args.containsKey("Team"))
			team = (String)args.get("Team");
		try {
			if(args.containsKey("Items"))
				for(Object s : (List<SerializableItemStack>)args.get("Items"))
					items.add(((SerializableItemStack)s).toItemStack());
			if(args.containsKey("Armour"))
				for(Object s : (List<SerializableArmourItemStack>)args.get("Armour"))
					armour.add(((SerializableArmourItemStack)s).toItemStack());
			if(args.containsKey("Potion Effects"))
				for(Object s : (List<SerializablePotionEffect>)args.get("Potion Effects"))
					potionEffects.add(((SerializablePotionEffect)s).toPotionEffect());
			if(items.isEmpty())
    			for(Object s : api.getFiles().getDefaultConfig().getList("Items"))
					if(s != null)
						items.add((ItemStack)s);
    		if(armour.isEmpty())
    			for(Object s : api.getFiles().getDefaultConfig().getList("Armour"))
    				if(s != null)
						armour.add((ItemStack)s);
    		if(potionEffects.isEmpty())
    			for(Object s : api.getFiles().getDefaultConfig().getList("Potion Effects"))
    				if(s != null)
						potionEffects.add((PotionEffect)s);
    		return new FullClass(api, permission, name, description, team, items.toArray(new ItemStack[items.size()]), armour.toArray(new ItemStack[armour.size()]), potionEffects.toArray(new PotionEffect[potionEffects.size()]));
		} catch (Exception e) {
			Utils.getInstance().log.severe("{ClassAPI} An error occured parsing the configuration: ");
			e.printStackTrace();
			return new NullAbstractClass(api);
		}
	}
}