package me.sayaad.ClassAPI.classes;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

/**
 * @author Sayaad
 * @since 2013
 */

public interface Class extends ConfigurationSerializable{

	/**
	 * Gets the permission node configured for the specific class, cannot be null.
	 * @return String
	 */
	public String getPermissionNode();
	
	/**
	 * Gets the name configured for the specific class, cannot be null.
	 * @return String
	 */
	public String getName();
	
	/**
	 * Gets the description configured for the specific class.
	 * @return String
	 */
	public String getDescription();
	
	/**
	 * Gets the teamName configured for the specific class.
	 * @return String
	 */
	public String getTeam();
	
	/**
	 * Gets the various ItemStacks listed under, "Items" in the configuration file.
	 * @return ItemStack[]
	 */
	public ItemStack[] getItems();
	
	/**
	 * Gets the various ItemStacks listed under, "Armour" in the configuration file, cannot be null.
	 * @return ItemStack[]
	 */
	public ItemStack[] getArmour();
	
	/**
	 * Gets the various Potions listed under, "Potions" in the configuration file, as an org.bukkit.potion.PotionEffect
	 * @see <a href=http://jd.bukkit.org/rb/apidocs/org/bukkit/potion/PotionEffect.html>
	 * @return PotionEffect[]
	 */
	public PotionEffect[] getPotionEffects();
	
}