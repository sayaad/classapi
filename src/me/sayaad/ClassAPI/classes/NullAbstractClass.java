package me.sayaad.ClassAPI.classes;

import me.sayaad.ClassAPI.ClassAPI;
import me.sayaad.ClassAPI.constants.DefaultValues;

/**
 * Default Class.
 * @author Sayaad
 * @since 2013
 */

public class NullAbstractClass extends AbstractClass{
	
	/**
	 * Construct a class with only the default values. Usually called to prevent NullPointerExceptions and to have support for default variables.
	 * @param api ClassAPI to obtain default values from.
	 */
	public NullAbstractClass(ClassAPI api) throws NullPointerException {
		super(api, DefaultValues.getDefaultString(DefaultValues.DEFAULT_PERMISSION, api), DefaultValues.getDefaultString(DefaultValues.DEFAULT_NAME, api), DefaultValues.getDefaultItemStack(DefaultValues.DEFAULT_ITEMS, api));
	}
}
