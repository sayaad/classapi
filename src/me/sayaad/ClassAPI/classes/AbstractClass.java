package me.sayaad.ClassAPI.classes;

import me.sayaad.ClassAPI.ClassAPI;
import me.sayaad.ClassAPI.constants.DefaultValues;

import org.bukkit.inventory.ItemStack;

/**
 * Minimal featured class.
 * @author Sayaad
 * @since 2013
 */
public class AbstractClass extends FullClass{
	
	/**
	 * Construct a class with only the bare minimum required.
	 * @param permission Permission node required to obtain the class.
	 * @param name The name of the class.
	 * @param items Items to be given.
	 * @throws NullPointerException The configuration is null.
	 */
	public AbstractClass(ClassAPI api, String permission, String name, ItemStack[] items) throws NullPointerException{
		super(api, permission, name, DefaultValues.getDefaultString(DefaultValues.DEFAULT_DESCRIPTION, api), DefaultValues.getDefaultString(DefaultValues.DEFAULT_TEAM, api), items, DefaultValues.getDefaultItemStack(DefaultValues.DEFAULT_ARMOUR, api), DefaultValues.getDefaultPotionEffects(api));
	}
}