package me.sayaad.ClassAPI.exceptions;

public class ClassNotFoundException extends Exception{

	private static final long serialVersionUID = 42L;

	public ClassNotFoundException(String message){
		super(message); 
	}
}
