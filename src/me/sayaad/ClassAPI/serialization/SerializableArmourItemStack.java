package me.sayaad.ClassAPI.serialization;

import java.util.Map;

import org.bukkit.Color;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

@SerializableAs("Armour")
public class SerializableArmourItemStack extends SerializableItemStack{

	public SerializableArmourItemStack(ItemStack is) {
		super(is);
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> map = super.serialize(); 
		if(hasItemMeta())
			if(getItemMeta() instanceof LeatherArmorMeta)
				map.put("Colour", ((LeatherArmorMeta)getItemMeta()).getColor());
		return map;
	}
	
	public static SerializableArmourItemStack deserialize(Map<String, Object> map){
		ItemStack is = SerializableItemStack.deserialize(map).toItemStack();
		if(is.getType().name().contains("LEATHER_")){
			if(map.containsKey("Colour")){
				LeatherArmorMeta im = (LeatherArmorMeta)is.getItemMeta();
				im.setColor((Color)map.get("Colour"));
				is.setItemMeta(im);
			}
		}
		return new SerializableArmourItemStack(is);
	}
}
