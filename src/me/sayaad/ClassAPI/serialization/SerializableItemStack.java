package me.sayaad.ClassAPI.serialization;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

@SerializableAs("Item")
public class SerializableItemStack extends ItemStack{
	
	public SerializableItemStack(ItemStack is){
		super(is);
	}
	
	public ItemStack toItemStack(){
		ItemStack is = new ItemStack(getType(), getAmount(), getDurability());
		is.setItemMeta(getItemMeta());
		return is;
	}
	
	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("Type", getType().name());
		if(getAmount() > 1)
			map.put("Amount", getAmount());
		if(getDurability() > 0)
			map.put("Data", String.valueOf(getDurability()));
		if(!hasItemMeta())
			return map;
		if(getItemMeta().getDisplayName() != null)
			map.put("Display Name", getItemMeta().getDisplayName().replaceAll("§§", "&&").replaceAll("§", "&"));
		List<String> enchants = new ArrayList<>();
		for(Enchantment enchantment : getEnchantments().keySet())
			enchants.add(enchantment.getName() + ":" + getEnchantments().get(enchantment));
		if(!enchants.isEmpty())
			map.put("Enchants", enchants);
		if(getItemMeta().getLore() != null)
			map.put("Lore", getItemMeta().getLore());
		return map;
	}
	
	@SuppressWarnings("unchecked")
	public static SerializableItemStack deserialize(Map<String, Object> map){
		ItemStack is = new ItemStack(Material.valueOf((String)map.get("Type")), map.containsKey("Amount")? (int)map.get("Amount") : 1, map.containsKey("Data")? Short.valueOf((String)map.get("Data")) : 0);
		ItemMeta im = is.getItemMeta();
		if(im == null)
			return new SerializableItemStack(is);
		if(map.containsKey("Display Name"))
			im.setDisplayName((String)map.get("Display Name"));
		if(map.containsKey("Enchants"))
			for(String s : (List<String>)map.get("Enchants"))
				im.addEnchant(Enchantment.getByName(s.split(":")[0]), Integer.valueOf(s.split(":")[1]), true);
		if(map.containsKey("Lore"))
			im.setLore((List<String>)map.get("Lore"));
		is.setItemMeta(im);
		return new SerializableItemStack(is);
	}
}
