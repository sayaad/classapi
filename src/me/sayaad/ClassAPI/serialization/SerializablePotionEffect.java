package me.sayaad.ClassAPI.serialization;

import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

@SerializableAs("Potion")
public class SerializablePotionEffect extends PotionEffect{

	public SerializablePotionEffect(PotionEffect pe) {
		super(pe.getType(), pe.getDuration(), pe.getAmplifier());
	}
	
	public SerializablePotionEffect(Map<String, Object> map) {
		super(PotionEffectType.getByName((String)map.get("Type")), (int)map.get("Duration"), (int)map.get("Amplifier"));
	}
	
	public PotionEffect toPotionEffect(){
		return new PotionEffect(getType(), getDuration(), getAmplifier());
	}
	
	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("Type", getType().getName());
		map.put("Duration", getDuration());
		map.put("Amplifier", getAmplifier());
		return map;
	}
}
