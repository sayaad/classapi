package me.sayaad.ClassAPI.constants;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import me.sayaad.ClassAPI.ClassAPI;
import me.sayaad.ClassAPI.serialization.SerializableArmourItemStack;
import me.sayaad.ClassAPI.serialization.SerializableItemStack;
import me.sayaad.ClassAPI.serialization.SerializablePotionEffect;

/**
 * Enum to facilitate the getting of default values.
 * @author Sayaad
 * @since 2013
 */

public enum DefaultValues {

	DEFAULT_PERMISSION,
	DEFAULT_NAME,
	DEFAULT_DESCRIPTION,
	DEFAULT_TEAM,
	DEFAULT_ITEMS,
	DEFAULT_ARMOUR,
	DEFAULT_POTION_EFFECTS;
	
	/**
	 * Gets any String object of the default class, as specified by valueType.
	 * @param valueType The string to return.
	 * @param api ClassAPI to obtain files etc. from.
	 * @return String
	 */
	public static synchronized String getDefaultString(DefaultValues valueType, ClassAPI api){
		return api.getFiles().getDefaultConfig().getString(valueType.name().split("DEFAULT_")[1].toLowerCase().substring(0, 1).toUpperCase() + valueType.name().split("DEFAULT_")[1].toLowerCase().substring(1));
	}
	
	/**
	 * Gets any ItemStack[] object of the default class, as specified by valueType.
	 * @param valueType The string to return.
	 * @param api ClassAPI to obtain files etc. from.
	 * @return ItemStack[]
	 */
	@SuppressWarnings({"incomplete-switch", "static-access"})
	public static synchronized ItemStack[] getDefaultItemStack(DefaultValues valueType, ClassAPI api){
		try{
			switch(valueType){
				case DEFAULT_ITEMS:
					List<ItemStack> is = new ArrayList<>();
					for(Object s : api.getFiles().getDefaultConfig().getList("Items"))
						if(s != null)
							is.add(((SerializableItemStack)s).toItemStack());
					return is.toArray(new ItemStack[is.size()]);
				case DEFAULT_ARMOUR:
					List<ItemStack> Is = new ArrayList<>();
					for(Object s : api.getFiles().getDefaultConfig().getList("Armour"))
						if(s != null)
							Is.add(((SerializableArmourItemStack)s).toItemStack());
					return Is.toArray(new ItemStack[Is.size()]);
			}
			return null;
		}catch(Exception e){
			Utils.getInstance().log.severe("{ClassAPI} Unable to get the default value " + valueType.name());
			return null;
		}
	}
	
	/**
	 * Gets any PotionEffect[] object of the default class, as specified by valueType.
	 * @param valueType The string to return.
	 * @param api ClassAPI to obtain files etc. from.
	 * @return PotionEffect[]
	 */
	@SuppressWarnings("static-access")
	public static synchronized PotionEffect[] getDefaultPotionEffects(ClassAPI api){
		try{
			List<PotionEffect> iS = new ArrayList<>();
			for(Object s : api.getFiles().getDefaultConfig().getList("Potion Effects"))
				if(s != null)
					iS.add(((SerializablePotionEffect)s).toPotionEffect());
			return iS.toArray(new PotionEffect[iS.size()]);
		}catch(Exception e){
			Utils.getInstance().log.severe("{ClassAPI} Unable to get the default Potion Effects");
			return null;
		}
	}
}