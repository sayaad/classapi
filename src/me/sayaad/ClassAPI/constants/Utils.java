package me.sayaad.ClassAPI.constants;

import java.util.logging.Logger;

/**
 * ClassAPI Utilities thread-safe singleton. Used for the storing of useful Objects and Methods.
 * @author Sayaad
 * @since 2013
 */

public class Utils {

	private static Utils instance;
	
	public static Logger log = Logger.getLogger("Minecraft");
	
	private Utils(){}
	
	public static Utils getInstance(){
		if(instance == null){
			synchronized(Utils.class){
				instance = new Utils();
			}
		}
		return instance;
	}
}
