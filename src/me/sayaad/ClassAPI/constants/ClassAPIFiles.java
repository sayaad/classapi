package me.sayaad.ClassAPI.constants;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;

/**
 * ClassAPI's File manager. Primarily facilitates the location of configuration files.
 * @author Sayaad
 * @since 2013 
 */
public class ClassAPIFiles {

	private File mainDir;
	private File mainConfigFile;
	private File defaultClassinfoFile;
	
	/**
	 * Create configuration files for ClassAPI within a customisable directory.
	 * @param mainDir The directory to create/load configuration files in.
	 */
	public ClassAPIFiles(String mainDir){
		this();
		if(mainDir.equals(""))
			return;
		this.mainDir = new File(mainDir);
		mainConfigFile = new File(mainDir, "Classes.yml");
		defaultClassinfoFile = new File(mainDir, "Default Class.yml");
	}
	
	/**
	 * Executed if no specific directory for configuration files are given. Sets the files to their default directory: "plugin/ClassAPI/".
	 */
	private ClassAPIFiles(){
		mainDir = new File("plugins/ClassAPI/");
		mainConfigFile = new File(mainDir, "Classes.yml");
		defaultClassinfoFile = new File(mainDir, "Default Class.yml");
	}
	
	/**
	 * Get the information which would be default if no information of that configuration option is specified in the main configuration.
	 * @return File
	 */
	public synchronized File getDefaultClassInfoFile(){
		if(checkFile(mainDir, defaultClassinfoFile))
			return defaultClassinfoFile;
		else return null;
	}
	
	/**
	 * Get the file in which classes are configured.
	 * @return File
	 */
	public synchronized File getMainConfigFile(){
		if(checkFile(mainDir, mainConfigFile))
			return mainConfigFile;
		else return null;
	}
	
	/**
	 * Gets the YamlConfiguration for the main configuration file. If the file is empty, the default configuration for it would be copied onto it.
	 * @return YamlConfiguration
	 */
	@SuppressWarnings("static-access")
	public synchronized YamlConfiguration getDefaultConfig(){
		YamlConfiguration config = new YamlConfiguration();
		try {
			if(isEmpty(getDefaultClassInfoFile())){
				config.options().copyDefaults(true);
		        config.setDefaults(YamlConfiguration.loadConfiguration(getClass().getClassLoader().getResourceAsStream("Default Class.yml")));
		        config.save(getDefaultClassInfoFile());
			}
			config.load(getDefaultClassInfoFile());
		} catch (Exception e) {
			Utils.getInstance().log.severe("{ClassAPI} Error occured upon getting the Default Config!\n");
			System.out.println("Message: " + e.getLocalizedMessage());
			System.out.println("Cause: " + e.getCause());
			e.printStackTrace();
		}
		return config;
	}
	
	/**
	 * Gets the YamlConfiguration for the default configuration file. If the file is empty, the default configuration for it would be copied onto it.
	 * @return YamlConfiguration
	 */
	@SuppressWarnings("static-access")
	public synchronized YamlConfiguration getMainConfig(){
		YamlConfiguration config = new YamlConfiguration();
		try {
			if(isEmpty(getMainConfigFile())){
				config.options().copyDefaults(true);
		        config.setDefaults(YamlConfiguration.loadConfiguration(getClass().getClassLoader().getResourceAsStream("Classes.yml")));
		        config.save(getMainConfigFile());
			}
			config.load(getMainConfigFile());
		} catch (Exception e) {
			Utils.getInstance().log.severe("{ClassAPI} Severe error occured upon getting the Main Config!\n");
			System.out.println("Message: " + e.getLocalizedMessage());
			System.out.println("Cause: " + e.getCause());
			e.printStackTrace();
		}
		return config;
	}
	
	/**
	 * Check if the file exists, if not create it.
	 * @param f File to be checked.
	 * @exception IOException If an error occurred, return false.
	 */
	public synchronized boolean checkFile(File mainDir, File f){
		try{
			if(!mainDir.exists())
				mainDir.mkdir();
			if(!f.exists())
				return f.createNewFile();
			return true;
		}catch(IOException e){
			return false;
		}
	}
	
	/**
	 * Checks if a file f is empty. If an exception is thrown, assume that the file is empty.
	 * @param f File to check.
	 * @return boolean
	 * @throws IOException Assume the file is empty.
	 */
	@SuppressWarnings("resource")
	public synchronized boolean isEmpty(File f){
		try {
			BufferedReader br = new BufferedReader(new FileReader(f));
			if (br.readLine() == null) 
				return true;
			return false;
		} catch (IOException e) {
			return true;
		}
	}
}